﻿using App.Entity;
using App.IServices;
using App.Library;
using App.Site.Filters;
using FluentValidation;
using FluentValidation.Results;
using System;
using System.Web.Mvc;

namespace App.Site.Areas.Backend.Controllers
{
    // 跳过登录验证
    [SkipLogin]
    public class EntryController : BaseController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public EntryController(IUserCoreServices _IUserCoreServices)
        {
            base._IUserCoreServices = _IUserCoreServices;
        }

        [HttpGet]
        public ActionResult Index()
        {
            if (Session[AppKey.UserSessionKey] != null)
            {
                return RedirectToAction("Index", "Default");
            }
            return View();
        }

        // 登录
        [HttpPost]
        public ActionResult Login(UserCoreEntity userModel)
        {
            // 表单验证
            UserCoreEntityValidator validator = new UserCoreEntityValidator();
            ValidationResult results = validator.Validate(userModel, ruleSet: "Login");
            if (!results.IsValid)
            {
                return Json(TipMsg.CreateValidateFailMsg(results));
            }

            // 查询数据库
            userModel.Account = userModel.Account.Trim();
            userModel.Password = userModel.Password.Trim().ToMD5Lower();
            userModel = _IUserCoreServices.QuerySingle(u => u.Account == userModel.Account && u.Password == userModel.Password);
            if (userModel == null)
            {
                return Json(TipMsg.CreateResponseMsg("no_data", "账号或密码错误！"));
            }
            else
            {
                if (userModel.IsDel == false && userModel.IsExpire == false)
                {
                    // 变更状态
                    userModel.LastLoginTime = userModel.CurrentLoginTime;
                    userModel.LastLoginIPAddress = userModel.CurrentLoginIPAddress;
                    userModel.CurrentLoginTime = DateTime.Now;
                    userModel.CurrentLoginIPAddress = SystemHelper.GetIP();
                    userModel.LoginCount = userModel.LoginCount + 1;
                    userModel.UpdateCount = userModel.UpdateCount + 1;
                    userModel.UpdateTime = DateTime.Now;
                    userModel.UpdateIPAddress = SystemHelper.GetIP();

                    // 更新到数据库
                    _IUserCoreServices.Update(userModel);

                    // 写入Session
                    Session[AppKey.UserSessionKey] = userModel;

                    return Json(TipMsg.CreateResponseMsg("success", "登录成功，稍后跳转..."));
                }
                else
                {
                    return Json(TipMsg.CreateResponseMsg("no_normal", "账号已过期或已删除！"));
                }
            }
        }

        // 退出登录
        [HttpGet]
        public ActionResult Exit()
        {
            Session[AppKey.UserSessionKey] = null;
            Session.Abandon();
            return RedirectToAction("Index", "Entry");
        }
    }
}